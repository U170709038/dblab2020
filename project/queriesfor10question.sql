/* What is the average parking capacity of the car parks? */
select avg(property_value) as 'Avarage Parking Capacity'
from park_properties join properties on park_properties.property_id = properties.property_id
where property_name = "Park Kapasitesi";

/* What is the average of open car park’s hours of the car parks? */
select round(avg(left(hour_end-hour_start, 2))) as 'Average work hours of open car parking space' 
from working_hours join park_infos on working_hours.working_hours_id = park_infos.working_hours_id
where park_id in (
	select park_id
    from park_infos join park_type on park_infos.type_id = park_type.type_id
    where type_name = "Açık Otopark"
);

/* What is the maximum price of car parking for all day? */
select max(price_value) as 'Maximum price of Parking for All Day'
from park_hourly_prices join hourly_prices on park_hourly_prices.price_id = hourly_prices.price_id
where period_start = 0 and period_end = 24;

/* What is the minimum car parking capacity? */
select min(property_value) as 'Minumum Parking Capacity'
from park_properties join properties on park_properties.property_id = properties.property_id
where property_name = "Park Kapasitesi";

/* What is the maximum price of monthly subscription of car park? */
select max(property_value) as 'Maximum price of Monthly Subs.'
from park_properties join properties on park_properties.property_id = properties.property_id
where property_name = "Aylık Abonelik Ücreti";

/* Which district has maximum car parking spaces? */
select district_name
from district
where district_id = (
	select district_id
	from location
	where location_id =(
		select location_id
		from geographic_info
		where geographic_id = (
			select geographic_id
			from park_infos
			where park_id = (
				select max(park_id)
				from park_properties join properties on park_properties.property_id = properties.property_id
				where property_name = "Park Kapasitesi"))));

/* What is the average of monthly subscription of car park? */
select avg(property_value) as 'Average price of Monthly Subs.'
from park_properties join properties on park_properties.property_id = properties.property_id
where property_name = "Aylık Abonelik Ücreti";

/* What is the maximum time of free parking? */
select max(property_value) as 'Maximum time of free parking'
from park_properties join properties on park_properties.property_id = properties.property_id
where property_name = "Ücretsiz Parklama Süresi";

/* What is the average work hours of car parking space? */
select round(avg(left(hour_end-hour_start, 2))) as 'Average work hours of car parking space' 
from working_hours;

/* What is average of open car parking spaces? */
select CONCAT('%', round((sum(type_name = "Açık Otopark") / count(park_id)) * 100)) as 'Average of open car parking spaces'
from park_infos join park_type on park_infos.type_id = park_type.type_id;

/* Show count of existing sub regions of region*/
select region_name as 'Region Name', count(sub_region_id) as 'Count of Sub Region'
from regions join sub_regions on regions.region_id = sub_regions.region_id group by region_name;


